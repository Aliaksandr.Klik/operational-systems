#include <algorithm>
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>
#include "windows.h"

std::mutex mtx;

void min_max(std::vector<int>& array, int& min, int& max) {
    min = array[0];
    max = array[0];
    for (int i = 1; i < array.size(); i++) {
        std::lock_guard<std::mutex> lock(mtx);
        if (array[i] < min) min = array[i];
        Sleep(7);
        if (array[i] > max) max = array[i];
        Sleep(7);
    }
    std::cout << "Min: " << min << std::endl;
    std::cout << "Max: " << max << std::endl;
}

void average(std::vector<int>& array, int& average) {
    int sum = 0;
    for (int i : array) {
        std::lock_guard<std::mutex> lock(mtx);
        sum += i;
        Sleep(12);
    }
    average = sum / array.size();
    std::cout << "Average: " << average << std::endl;
}

int main() {
    int size;
    std::cout << "Enter array size: ";
    std::cin >> size;
    std::vector<int> array(size);
    std::cout << "Enter array elements: ";
    for (int & i : array) std::cin >> i;

    int min, max, average_val;
    std::thread min_max_thread(min_max, std::ref(array), std::ref(min), std::ref(max));
    std::thread average_thread(average, std::ref(array), std::ref(average_val));

    min_max_thread.join();
    average_thread.join();

    std::replace(array.begin(), array.end(), max, average_val);
    std::replace(array.begin(), array.end(), min, average_val);

    std::cout << "New array: ";
    for (int i : array) std::cout << i << " ";
    std::cout << std::endl;
}