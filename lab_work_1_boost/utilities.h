#ifndef UTILITIES_H
#define UTILITIES_H

#include <functional>
#include <string>

template<class T>
using ValidationFunction = std::function<bool(const T&)>;

bool positiveIntegerStringValidator(const std::string&);

bool positiveDoubleStringValidator(const std::string&);

template<class T>
T requestValue(const std::string& requestedItemName, ValidationFunction<T> validator);

#endif
