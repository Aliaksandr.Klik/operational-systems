#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN

#include <boost/test/unit_test.hpp>

#include "utilities.h"

BOOST_AUTO_TEST_SUITE(utilities_test_suite)

BOOST_AUTO_TEST_CASE(positive_integer_string_validator_test) {
    BOOST_CHECK(!positiveIntegerStringValidator(""));
    BOOST_CHECK(!positiveIntegerStringValidator("a"));
    BOOST_CHECK(!positiveIntegerStringValidator("0.0"));
    BOOST_CHECK(!positiveIntegerStringValidator("0"));
    BOOST_CHECK(!positiveIntegerStringValidator("-1"));
    BOOST_CHECK(positiveIntegerStringValidator("123"));
    BOOST_CHECK(positiveIntegerStringValidator("999999"));
}

BOOST_AUTO_TEST_CASE(positive_double_string_validator_test) {
    BOOST_CHECK(!positiveDoubleStringValidator(""));
    BOOST_CHECK(!positiveDoubleStringValidator("a"));
    BOOST_CHECK(!positiveDoubleStringValidator("0.0.0"));
    BOOST_CHECK(!positiveDoubleStringValidator("0"));
    BOOST_CHECK(!positiveDoubleStringValidator("-1"));
    BOOST_CHECK(positiveDoubleStringValidator("0.1"));
    BOOST_CHECK(positiveDoubleStringValidator("8"));
    BOOST_CHECK(positiveDoubleStringValidator("999999"));
}

BOOST_AUTO_TEST_SUITE_END()
