#include <fstream>
#include <iostream>
#include <string>

#include "boost/process.hpp"
#include "boost/filesystem.hpp"

#include "Employee.h"
#include "utilities.h"

int main() {
    auto fileName = requestValue<std::string>("binary file name",boost::filesystem::native);
    auto recordsCount = requestValue<std::string>("amount of records", positiveIntegerStringValidator);
    std::string creatorPath = std::string("Creator") + FILE_POSTFIX + " " + fileName + " " + recordsCount;
    try {
        boost::process::system(creatorPath);
    } catch (std::exception&) {
        std::cout << "Failed to create process" << std::endl;
        return 1;
    }
    std::fstream file;
    try {
        file.open(fileName, std::ios::in | std::ios::binary);
        if (!file.is_open()) {
            std::cout << "Failed to open file" << std::endl;
            return 1;
        }
        Employee employee;
        while (file.read((char *) &employee, sizeof(Employee))) {
            std::cout << employee.num << " " << employee.name << " " << employee.hours << std::endl;
        }
        file.close();
    }
    catch (std::exception&) {
        std::cout << "Failed to read from file" << std::endl;
        return 1;
    }
    auto reportFileName = requestValue<std::string>(
            "report file name",
            [fileName](const std::string& value) {
                return boost::filesystem::native(value) && value != fileName;
            }
    );
    auto paymentPerHour = requestValue<std::string>("payment per hour", positiveDoubleStringValidator);

    std::string reporterPath = std::string("Reporter") + FILE_POSTFIX + " " + fileName + " " + reportFileName + " " + paymentPerHour;
    boost::process::child reporter(reporterPath);
    reporter.wait();
    std::fstream reportFile;
    try {
        reportFile.open(reportFileName);
        if (!reportFile.is_open()) {
            std::cout << "Failed to open report file" << std::endl;
            return 1;
        }
        std::string line;
        while (std::getline(reportFile, line)) {
            std::cout << line << std::endl;
        }
        reportFile.close();
    }
    catch (std::exception&) {
        std::cout << "Failed to read from report file" << std::endl;
        return 1;
    }
    return 0;
}
