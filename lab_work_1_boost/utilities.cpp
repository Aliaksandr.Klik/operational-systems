#include "utilities.h"

#include <algorithm>
#include <iostream>
#include <limits>

bool positiveIntegerStringValidator(const std::string &value) {
    return !value.empty() && std::all_of(value.begin(), value.end(), ::isdigit) && value[0] != '0';
}

bool positiveDoubleStringValidator(const std::string &value) {
    try {
        return std::stod(value) > 0;
    } catch (std::exception&) {
        return false;
    }
}

template<class T>
T requestValue(const std::string& requestedItemName, ValidationFunction<T> validator) {
    T value{};
    std::cout << "Please enter " << requestedItemName << ":" << std::endl;
    do {
        std::cin >> value;
        if (!std::cin.good() || !validator(value)) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Invalid " << requestedItemName << ". Please try again:" << std::endl;
        }
    } while (!validator(value));
    return value;
}

template int requestValue<int>(const std::string&, ValidationFunction<int>);
template std::string requestValue<std::string>(const std::string&, ValidationFunction<std::string>);
template bool requestValue<bool>(const std::string&, ValidationFunction<bool>);
template double requestValue<double>(const std::string&, ValidationFunction<double>);