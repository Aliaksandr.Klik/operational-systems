#include <fstream>
#include <iostream>
#include <string>

#include "Employee.h"
#include "utilities.h"

int main(int argc, char *argv[]) {
    if (argc != 4) {
        std::cout << "Invalid number of arguments" << std::endl;
        std::cout << "Try using Main.exe app instead, or calling this program with 3 arguments:" << std::endl;
        std::cout << "1. Binary file name" << std::endl;
        std::cout << "2. Report file name" << std::endl;
        std::cout << "3. Payment per hour" << std::endl;
        return 1;
    }
    char *fileName = argv[1];
    char *reportFileName = argv[2];
    double paymentPerHour;
    try {
        paymentPerHour = std::atof(argv[3]);
        if (paymentPerHour < 0) {
            std::cout << "Invalid payment per hour" << std::endl;
            return 1;
        }
    }
    catch (std::invalid_argument&) {
        std::cout << "Invalid payment per hour" << std::endl;
        return 1;
    }
    catch (std::out_of_range&) {
        std::cout << "Invalid payment per hour" << std::endl;
        return 1;
    }
    std::fstream file;
    try {
        file.open(fileName, std::ios::in | std::ios::binary);
        if (!file.is_open()) {
            std::cout << "Failed to open file" << std::endl;
            return 1;
        }
        std::ofstream reportFile;
        reportFile.open(reportFileName);
        if (!reportFile.is_open()) {
            std::cout << "Failed to open report file" << std::endl;
            return 1;
        }
        reportFile << "Report for file " << fileName << ":" << std::endl << std::endl;
        Employee employee;
        while (file.read((char *) &employee, sizeof(Employee))) {
            reportFile << "Id: " << employee.num << std::endl;
            reportFile << "Name: " << employee.name << std::endl;
            reportFile << "Work hours: " << employee.hours << std::endl;
            reportFile << "Salary: " << employee.hours * paymentPerHour << std::endl;
            reportFile << std::endl;
        }
        file.close();
        reportFile.close();
    }
    catch (std::exception&) {
        std::cout << "Failed to read from file" << std::endl;
        return 1;
    }
    return 0;
}