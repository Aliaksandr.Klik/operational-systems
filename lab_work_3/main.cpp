#include <algorithm>
#include <climits>
#include <iostream>
#include <random>

#include <windows.h>

CRITICAL_SECTION cs;

int arraySize = 0;
int* array = nullptr;

int markerCount = 0;
HANDLE* markerHandles = nullptr;
DWORD* markerIds = nullptr;
HANDLE* markerRequestHandles = nullptr;

HANDLE continueEvent;
HANDLE exitEvent;

int exitId = 0;

void setupArray();
void setupMarkers();
void setup();
void cleanup();
void unmarkArray(int id);
void printArray();
void getExitId();
DWORD WINAPI marker(LPVOID id);
void mainLoop();

void setupArray() {
    std::cout << "Enter the array size. It must be a number between 1 and " << INT_MAX << std::endl;
    do {
        std::cin >> arraySize;
        if (std::cin.fail() || arraySize <= 0) {
            std::cin.clear();
            std::cin.ignore(INT_MAX, '\n');
            std::cout << "Invalid input. Try again." << std::endl;
        }
    } while (arraySize <= 0);
    array = new int[arraySize];
    std::fill(array, array + arraySize, 0);
}

void setupMarkers() {
    std::cout << "Enter the amount of markers. It must be a number between 1 and " << INT_MAX << std::endl;
    do {
        std::cin >> markerCount;
        if (std::cin.fail() || markerCount <= 0) {
            std::cin.clear();
            std::cin.ignore(INT_MAX, '\n');
            std::cout << "Invalid input. Try again." << std::endl;
        }
    } while (markerCount <= 0);
    markerHandles = new HANDLE[markerCount];
    markerIds = new DWORD[markerCount];
    markerRequestHandles = new HANDLE[markerCount];
}

void setup() {
    setupArray();
    setupMarkers();
    InitializeCriticalSection(&cs);

    for (int i = 0; i < markerCount; ++i) {
        markerRequestHandles[i] = CreateEvent(nullptr, TRUE, FALSE, nullptr);
    }

    continueEvent = CreateEvent(nullptr, TRUE, FALSE, nullptr);
    exitEvent = CreateEvent(nullptr, TRUE, FALSE, nullptr);

    for (int i = 0; i < markerCount; ++i) {
        markerHandles[i] = CreateThread(nullptr, 0, marker, (LPVOID)i, 0, &markerIds[i]);
        if (markerHandles[i] == nullptr) {
            std::cout << "Error creating thread number " << i + 1 << std::endl;
        }
    }
}

void cleanup() {
    delete[] array;

    DeleteCriticalSection(&cs);

    for (int i = 0; i < markerCount; ++i) {
        CloseHandle(markerRequestHandles[i]);
    }
    delete[] markerHandles;
    delete[] markerIds;
    delete[] markerRequestHandles;

    CloseHandle(continueEvent);
    CloseHandle(exitEvent);
}

void unmarkArray(int id) {
    EnterCriticalSection(&cs);
    for (int i = 0; i < arraySize; ++i) {
        if (array[i] == id) {
            array[i] = 0;
        }
    }
    LeaveCriticalSection(&cs);
}

void printArray() {
    EnterCriticalSection(&cs);
    std::cout << std::endl;
    for (int i = 0; i < arraySize; ++i) {
        std::cout << array[i] << " ";
    }
    std::cout << std::endl;
    LeaveCriticalSection(&cs);
}

void getExitId() {
    exitId = 0;
    std::cout << "Enter the id of the marker that you want to exit. It must be a number between 1 and " << markerCount << std::endl;
    do {
        std::cin >> exitId;
        if (std::cin.fail() || exitId <= 0 || exitId > markerCount) {
            std::cin.clear();
            std::cin.ignore(INT_MAX, '\n');
            std::cout << "Invalid input. Try again." << std::endl;
        } else if (WaitForSingleObject(markerHandles[exitId - 1], 0) != WAIT_TIMEOUT) {
            std::cout << "This marker is already closed. Try another marker." << std::endl;
            exitId = 0;
        }
    } while (exitId <= 0 || exitId > markerCount);
}

DWORD WINAPI marker(LPVOID id) {
    int markerId = (int)id;
    int publicId = markerId + 1;
    std::mt19937 rng(publicId);
    std::uniform_int_distribution<int> dist(0, arraySize - 1);

    WaitForSingleObject(continueEvent, INFINITE);

    int markedCounter = 0;

    while (true) {
        int r = dist(rng);
        if (array[r] == 0) {
            Sleep(5);
            array[r] = publicId;
            markedCounter++;
            Sleep(5);
        } else {
            EnterCriticalSection(&cs);
            std::cout << "Marker id: " << publicId << std::endl;
            std::cout << "The amount of marked elements: " << markedCounter << std::endl;
            std::cout << "Id of already marked element: " << r << std::endl;
            std::cout << std::endl;
            SetEvent(markerRequestHandles[markerId]);
            LeaveCriticalSection(&cs);

            WaitForSingleObject(exitEvent, INFINITE);

            if (exitId == publicId) {
                unmarkArray(publicId);
                return 0;
            }

            EnterCriticalSection(&cs);
            ResetEvent(markerRequestHandles[markerId]);
            LeaveCriticalSection(&cs);

            WaitForSingleObject(continueEvent, INFINITE);
            markedCounter = 0;
        }
    }
}

void mainLoop() {
    int exitsLeft = markerCount;
    while (exitsLeft != 0) {
        Sleep(10);
        SetEvent(continueEvent);
        WaitForMultipleObjects(markerCount, markerRequestHandles, TRUE, INFINITE);
        ResetEvent(continueEvent);

        std::cout << "Array after marking:" << std::endl;
        printArray();
        getExitId();

        SetEvent(exitEvent);
        DWORD waitResult = WaitForSingleObject(markerHandles[exitId - 1], INFINITE);
        if (waitResult != WAIT_FAILED) {
            CloseHandle(markerHandles[exitId - 1]);
            exitsLeft--;
        }
        ResetEvent(exitEvent);
    }
    std::cout << "All markers are closed. Terminating..." << std::endl;
}

int main() {
    setup();
    mainLoop();
    cleanup();
}
