cmake_minimum_required(VERSION 3.26)
project(lab_work_3)

set(CMAKE_CXX_STANDARD 17)

add_executable(lab_work_3 main.cpp)
