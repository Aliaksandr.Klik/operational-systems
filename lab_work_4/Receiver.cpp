#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <boost/process.hpp>
#include <boost/signals2.hpp>
#include <boost/interprocess/sync/named_semaphore.hpp>

namespace bp = boost::process;
namespace bs2 = boost::signals2;

bool isNumber(const std::string& str) {
    return !str.empty() && std::all_of(str.begin(), str.end(), ::isdigit);
}

int main() {
    std::string fileName;
    std::cout << "Enter your file name" << std::endl;
    std::cin >> fileName;
    std::ofstream outfile(fileName, std::ios_base::trunc);

    if (!outfile.is_open()) {
        std::cout << "Couldn't create file" << std::endl;
        return 1;
    }

    outfile.close();

    std::string maxRecordNumStr;

    while (!isNumber(maxRecordNumStr)) {
        std::cout << "Enter a maximum number of records in your file" << std::endl;
        std::cin >> maxRecordNumStr;
    }

    int maxRecordNum = std::stoi(maxRecordNumStr);

    std::string senderCntStr;
    while (!isNumber(senderCntStr)) {
        std::cout << "Enter the number of senders" << std::endl;
        std::cin >> senderCntStr;
    }

    int senderCnt = std::stoi(senderCntStr);

    std::vector<bs2::signal<void()>> readySignals(senderCnt);

    boost::interprocess::named_semaphore hMaxNumRecordsSem(boost::interprocess::open_or_create, "hMaxNumRecordsSem", maxRecordNum);

    std::vector<bp::child> processes;

    for (int i = 0; i < senderCnt; i++) {
        std::string finalCommandStr = "Sender " + fileName + " event" + std::to_string(i) + " hMaxNumRecordsSem";

        bs2::connection connection = readySignals[i].connect([i] {
            std::cout << "Process " << i << " is ready!" << std::endl;
        });

        processes.emplace_back(finalCommandStr, bp::std_out > stdout);

        connection.disconnect();
        readySignals[i]();
    }

    char charMessage[21];
    std::string fileNameStr(fileName.begin(), fileName.end());
    std::ifstream fin;
    std::ofstream fout;

    bool ableToRead = false;

    while (true) {
        std::cout << "Enter read or exit" << std::endl;

        std::string option;
        std::cin >> option;

        if (option == "read") {
            while (!ableToRead) {
                fin.open(fileName, std::ios_base::binary);

                if (fin.peek() != EOF) {
                    ableToRead = true;
                }

                fin.close();
            }

            ableToRead = false;

            fin.open(fileName, std::ios_base::binary);
            fin.read(charMessage, 20);
            charMessage[20] = '\0';
            std::cout << charMessage << std::endl;

            char dop;
            std::string extraInfoStr;

            while (true) {
                dop = fin.get();
                if (fin.fail()) break;
                else extraInfoStr += dop;
            }

            fin.close();

            fout.open(fileName, std::ios_base::binary | std::ios_base::trunc);
            fout.write(extraInfoStr.c_str(), extraInfoStr.length());
            fout.close();

            hMaxNumRecordsSem.post();
        }
        else if (option == "exit") {
            return 0;
        }
        else {
            std::cout << "You entered the wrong command" << std::endl;
        }
    }

    return 0;
}
