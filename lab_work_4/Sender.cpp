#include <iostream>
#include <fstream>
#include <string>
#include <boost/interprocess/sync/named_semaphore.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>

std::streamsize fileSize(const std::string& fileName) {
    std::ifstream file(fileName, std::ios::binary | std::ios::ate);
    std::streamsize size = 0;

    if (file.is_open()) {
        size = file.tellg();
        file.close();
    }

    return size;
}

int main(int argc, char** args) {
    if (argc != 4) {
        std::cout << "Invalid number of arguments" << std::endl;
        return 1;
    }

    std::string fileName(args[1]);
    std::ofstream fout;

    boost::interprocess::named_semaphore hMaxNumRecordsSem(boost::interprocess::open_or_create, args[3], 0);
    boost::interprocess::named_mutex hReadySignal(boost::interprocess::open_or_create, args[2]);

    hReadySignal.unlock();

    std::string command;

    while (true) {
        std::cout << "enter send and a message or exit" << std::endl;
        getline(std::cin, command);

        if (command == "exit") {
            return 0;
        }

        boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock(hReadySignal);
        hMaxNumRecordsSem.wait();

        if (command.find(' ') != std::string::npos && command.substr(0, command.find(' ')) == "send") {
            std::string message = command.substr(command.find(' ') + 1);

            if (message.length() > 20) {
                std::cout << "message length should not be greater than 20" << std::endl;
                hMaxNumRecordsSem.post();
                continue;
            }

            fout.open(fileName, std::ios::binary | std::ios::app);
            fout.write(message.c_str(), 20);
            fout.close();
        }
        else {
            std::cout << "unknown command" << std::endl;
            hMaxNumRecordsSem.post();
        }
    }

    return 0;
}
