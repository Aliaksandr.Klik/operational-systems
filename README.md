# Лабораторные работы по предмету "Операционные системы"

* ## [Лабораторная работа №1](lab_work_1/README.md)

* ## [Лабораторная работа №2](lab_work_2/README.md)

* ## [Лабораторная работа №3](lab_work_3/README.md)

* ## [Лабораторная работа №4](lab_work_4/README.md)

* ## [Лабораторная работа №5](lab_work_5/README.md)
