#include <fstream>
#include <iostream>

#include "Employee.h"

int main(int argc, char *argv[]) {
    if (argc != 3) {
        std::cout << "Invalid number of arguments" << std::endl;
        std::cout << "Try using Main.exe app instead, or calling this program with 2 arguments:" << std::endl;
        std::cout << "1. Binary file name" << std::endl;
        std::cout << "2. Amount of records" << std::endl;
        return 1;
    }
    char *fileName = argv[1];
    int recordsCount;
    try {
        recordsCount = std::atoi(argv[2]);
        if (recordsCount < 0) {
            std::cout << "Invalid amount of records" << std::endl;
            return 1;
        }
    }
    catch (std::invalid_argument&) {
        std::cout << "Invalid amount of records" << std::endl;
        return 1;
    }
    catch (std::out_of_range&) {
        std::cout << "Invalid amount of records" << std::endl;
        return 1;
    }
    std::fstream file;
    try {
        file.open(fileName, std::ios::out | std::ios::binary);
        if (!file.is_open()) {
            std::cout << "Failed to open file" << std::endl;
            return 1;
        }
        for (int i = 0; i < recordsCount; i++) {
            Employee employee;
            std::cout << "Enter employee's id, name and work hours:" << std::endl;
            std::cin >> employee.num >> employee.name >> employee.hours;
            file.write((char *) &employee, sizeof(Employee));
        }
        file.close();
    }
    catch (std::exception&) {
        std::cout << "Failed to write to file" << std::endl;
        return 1;
    }
    return 0;
}