#include <fstream>
#include <iostream>
#include <string>
#include "windows.h"

#include "Employee.h"

int main() {
    std::string fileName, recordsCount;
    std::cout << "Enter binary file name:" << std::endl;
    std::cin >> fileName;
    std::cout << "Enter amount of records:" << std::endl;
    std::cin >> recordsCount;
    std::string creatorPath = "Creator.exe " + fileName + " " + recordsCount;
    STARTUPINFO siCreator;
    PROCESS_INFORMATION piCreator;
    ZeroMemory(&siCreator, sizeof(siCreator));
    siCreator.cb = sizeof(siCreator);
    ZeroMemory(&piCreator, sizeof(piCreator));
    if (!CreateProcess(nullptr, (LPSTR) creatorPath.c_str(), nullptr, nullptr, FALSE, 0, nullptr, nullptr, &siCreator, &piCreator)) {
        std::cout << "Failed to create process" << std::endl;
        return 1;
    }
    WaitForSingleObject(piCreator.hProcess, INFINITE);
    CloseHandle(piCreator.hProcess);
    CloseHandle(piCreator.hThread);
    std::fstream file;
    try {
        file.open(fileName, std::ios::in | std::ios::binary);
        if (!file.is_open()) {
            std::cout << "Failed to open file" << std::endl;
            return 1;
        }
        Employee employee;
        while (file.read((char *) &employee, sizeof(Employee))) {
            std::cout << employee.num << " " << employee.name << " " << employee.hours << std::endl;
        }
        file.close();
    }
    catch (std::exception&) {
        std::cout << "Failed to read from file" << std::endl;
        return 1;
    }
    std::string reportFileName, paymentPerHour;
    std::cout << "Enter report file name:" << std::endl;
    std::cin >> reportFileName;
    std::cout << "Enter payment per hour:" << std::endl;
    std::cin >> paymentPerHour;
    std::string reporterPath = "Reporter.exe " + fileName + " " + reportFileName + " " + paymentPerHour;
    STARTUPINFO siReporter;
    PROCESS_INFORMATION piReporter;
    ZeroMemory(&siReporter, sizeof(siReporter));
    siReporter.cb = sizeof(siReporter);
    ZeroMemory(&piReporter, sizeof(piReporter));
    if (!CreateProcess(nullptr, (LPSTR) reporterPath.c_str(), nullptr, nullptr, FALSE, 0, nullptr, nullptr, &siReporter, &piReporter)) {
        std::cout << "Failed to create process" << std::endl;
        return 1;
    }
    WaitForSingleObject(piReporter.hProcess, INFINITE);
    CloseHandle(piReporter.hProcess);
    CloseHandle(piReporter.hThread);
    std::fstream reportFile;
    try {
        reportFile.open(reportFileName);
        if (!reportFile.is_open()) {
            std::cout << "Failed to open report file" << std::endl;
            return 1;
        }
        std::string line;
        while (std::getline(reportFile, line)) {
            std::cout << line << std::endl;
        }
        reportFile.close();
    }
    catch (std::exception&) {
        std::cout << "Failed to read from report file" << std::endl;
        return 1;
    }
    return 0;
}
