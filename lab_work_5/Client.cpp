#include <iostream>
#include <boost/interprocess/sync/named_mutex.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/file_mapping.hpp>
#include <boost/interprocess/mapped_region.hpp>

#include "employee.h"

using namespace boost::interprocess;
using std::cout;
using std::cin;

int main() {
    try {
        named_mutex mutex(open_or_create, "ProcessMutex");

        file_mapping fileMap("SharedMemoryObject", read_write);
        mapped_region region(fileMap, read_write);

        while (true) {
            int chosen_option = 0;
            cout << "Change data - 1; \nRead data - 2; \nExit - 3 \n>>>";
            cin >> chosen_option;

            if (chosen_option == 3) {
                break;
            } else if (chosen_option == 1 || chosen_option == 2) {
                scoped_lock<named_mutex> lock(mutex);

                int ID;
                cout << "Input an ID of employee:\n>>>";
                cin >> ID;

                int message_to_send = ID * 10 + chosen_option;

                std::memcpy(region.get_address(), &message_to_send, sizeof(message_to_send));


                int* result = static_cast<int*>(region.get_address());

                cout << "ID of employee: " << result[0] << ".\n"
                     << "Name of employee: " << result[1] << ".\n"
                     << "Hours of employee: " << result[2] << ".\n";


                system("pause");

                message_to_send = 1;

                std::memcpy(region.get_address(), &message_to_send, sizeof(message_to_send));
            }
        }
    } catch (const std::exception& ex) {
        cout << "Exception: " << ex.what() << std::endl;
    }

    system("pause");

    return 0;
}
