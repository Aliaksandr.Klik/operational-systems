#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <iostream>

struct employee {
    int num; // идентификационный номер сотрудника
    char name[10]; // имя сотрудника
    double hours; // количество отработанных часов

    friend std::ostream& operator << (std::ostream& os, const employee& employee) {
        os << employee.num << " " << employee.name << " " << employee.hours;
        return os;
    }

    friend std::istream& operator >> (std::istream& os, employee& employee) {
        os >> employee.num >> employee.name >> employee.hours;
        return os;
    }
};

#endif
