#include <iostream>
#include <fstream>
#include <memory>
#include <vector>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/interprocess/sync/named_semaphore.hpp>

#include "employee.h"

using namespace boost::asio;

io_service ioService;
ip::tcp::acceptor acceptor(ioService);

std::vector<ip::tcp::socket> sockets;
std::vector<boost::thread> threads;

employee* emps;
int number_of_employees;
int number_of_clients;
std::string file_name;

std::unique_ptr<boost::interprocess::named_semaphore>* hSemaphores;

void operations(ip::tcp::socket& socket) {
    try {
        int message;
        while (true) {
            read(socket, buffer(&message, sizeof(message)));

            int ID = message / 10;
            int chosen_option = message % 10;

            if (chosen_option == 1) {
                hSemaphores[ID - 1]->wait();

                auto* emp_to_push = new employee();
                emp_to_push->num = emps[ID - 1].num;
                emp_to_push->hours = emps[ID - 1].hours;
                strcpy(emp_to_push->name, emps[ID - 1].name);

                write(socket, buffer(emp_to_push, sizeof(employee)));

                read(socket, buffer(emp_to_push, sizeof(employee)));
                emps[ID - 1].hours = emp_to_push->hours;
                strcpy(emps[ID - 1].name, emp_to_push->name);

                std::ofstream file(file_name);
                for (int i = 0; i < number_of_employees; i++)
                    file << emps[i].num << " " << emps[i].name << " " << emps[i].hours << "\n";
                file.close();

                int msg = 1;
                write(socket, buffer(&msg, sizeof(msg)));

                hSemaphores[ID - 1]->post();
            }
            else if (chosen_option == 2) {
                hSemaphores[ID - 1]->wait();

                employee* emp_to_push = new employee();
                emp_to_push->num = emps[ID - 1].num;
                emp_to_push->hours = emps[ID - 1].hours;
                strcpy(emp_to_push->name, emps[ID - 1].name);

                write(socket, buffer(emp_to_push, sizeof(employee)));

                int msg = 1;
                write(socket, buffer(&msg, sizeof(msg)));

                hSemaphores[ID - 1]->post();
            }
        }
    }
    catch (std::exception& e) {
        std::cerr << "Exception in thread: " << e.what() << std::endl;
    }
}

void start_server() {
    ip::tcp::endpoint endpoint(ip::tcp::v4(), 12345);
    acceptor.open(endpoint.protocol());
    acceptor.set_option(ip::tcp::acceptor::reuse_address(true));
    acceptor.bind(endpoint);
    acceptor.listen();

    for (int i = 0; i < number_of_clients; ++i) {
        ip::tcp::socket socket(ioService);
        acceptor.accept(socket);
        sockets.push_back(std::move(socket));

        threads.emplace_back([i]() {
            operations(sockets[i]);
        });
    }

    acceptor.close();
}

int main() {
    std::cout << "Input file name:\n";
    std::cin >> file_name;

    std::cout << "Input number of employees:\n";
    std::cin >> number_of_employees;

    emps = new employee[number_of_employees];

    for (int i = 0; i < number_of_employees; i++) {
        std::cout << "Input " << i + 1 << " employee id:\n";
        std::cin >> emps[i].num;
        std::cout << "Input employee name:\n";
        std::cin >> emps[i].name;
        std::cout << "Input employee hours:\n";
        std::cin >> emps[i].hours;
    }

    std::ofstream file(file_name);
    for (int i = 0; i < number_of_employees; i++)
        file << emps[i].num << " " << emps[i].name << " " << emps[i].hours << "\n";
    file.close();

    std::ifstream file_input(file_name);
    for (int i = 0; i < number_of_employees; i++) {
        int id;
        char name[10];
        double hours;
        file_input >> id >> name >> hours;
        std::cout << "\nID of employee: " << id << ".\nName of employee: " << name << ".\nHours of employee: " << hours << ".\n";
    }
    file_input.close();

    std::cout << "Input number of clients:\n";
    std::cin >> number_of_clients;

    hSemaphores = new std::unique_ptr<boost::interprocess::named_semaphore>[number_of_employees];

    for (int i = 0; i < number_of_employees; i++) {
        hSemaphores[i] = std::make_unique<boost::interprocess::named_semaphore>(boost::interprocess::open_or_create, "hSemaphore", 1);
    }

    start_server();

    for (auto& thread : threads) {
        thread.join();
    }

    std::cout << "All clients have ended their work.";

    file_input.open(file_name);
    for (int i = 0; i < number_of_employees; i++) {
        int id;
        char name[10];
        double hours;
        file_input >> id >> name >> hours;
        std::cout << "\nID of employee: " << id << ".\nName of employee: " << name << ".\nHours of employee: " << hours << ".\n";
    }
    file_input.close();

    system("pause");

    return 0;
}
